const fs = require('fs').promises;
const path = require('path');
const process = require('process');
const { authenticate } = require('@google-cloud/local-auth');
const { google } = require('googleapis');

const SCOPES = ["https://www.googleapis.com/auth/drive"];

// token.json contient le jeton d'accès de l'utilisateur, est créé la première fois que la connexion aboutit et est actualisé à chaque utilisation
const TOKEN_PATH = path.join(process.cwd(), 'token.json');
const CREDENTIALS_PATH = path.join(process.cwd(), 'credentials.json')

/**
 * Lis les identifiants authorisé précédamment depuis le fichier de sauvegarde
 * 
 * @return {Promise<OAuth2Client|null}
 */
async function loadSavedCredentialsIfExists() {
    try {
        const content = await fs.readFile(TOKEN_PATH);
        const credentials = JSON.parse(content);
        return google.auth.fromJSON(credentials)
    } catch (err) {
        return null;
    }
}

/**
 * Produit des identifiants compatible avec la connexion par JSON de la librairie Google
 * 
 * @param {OAuth2Client} client
 * @return {Promise<void>}
 */

async function saveCredentials(client) {
    const content = await fs.readFile(CREDENTIALS_PATH);
    const keys = JSON.parse(content)
    const key = keys.installed || keys.web
    const payload = JSON.stringify({
        type: 'authorized_user',
        client_id: key.client_id,
        client_secret: key.client_secret,
        refresh_token: client.credentials.refresh_token

    });
    await fs.writeFile(TOKEN_PATH, payload);
}

/**
 * Charge ou demande l'authorisation pour appeler l'API
 */
async function authorize() {
    let client = await loadSavedCredentialsIfExists();
    if (client) {
        return client;
    }
    client = await authenticate({
        scopes: SCOPES,
        keyfilePath: CREDENTIALS_PATH,
    });
    if (client.credentials) {
        await saveCredentials(client);
    }
    return client
}

/**
 * Liste jusqu'a 10 fichiers
 * 
 * @param {OAuth2Client} authClient Un client OAuth2 authorisé
 */

async function listFiles(authClient) {
    const drive = google.drive({version: "v3", auth: authClient});
    const res = await drive.files.list({
        pageSize: 10,
        fields: "nextPageToken, files(id, name)",
    })
    const files = res.data.files
    if (files.length == 0) {
        console.log("Aucun fichiers trouvés")
        return
    }

    console.log("Fichiers : ")
    files.map((files) => {
        console.log(`${files.name} (${files.id})`)
    })
}

authorize().then(listFiles).catch(console.error)

